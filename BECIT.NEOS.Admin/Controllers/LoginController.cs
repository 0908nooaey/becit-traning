﻿using BECIT.NEOS.Admin.BusinessLogic;
using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BECIT.NEOS.Admin.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken] //ป้องกันการ spam
        public ActionResult Index(string userid, string password)
        {
            var currentUser = new CurrentUserBL().Verify(userid, password);
            if (currentUser.HasPermission)
            {
                Session["CurrentUser"] = currentUser;
                ViewBag.userid = currentUser.userid;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                var messageResult = new MessageResultModel();
                messageResult.Style = "alert alert-warning";
                messageResult.Title = "Warning!";
                messageResult.Description = currentUser.LoginNotifymessage;
                TempData["MessageResult"] = messageResult;
                return View();
            }
        }

        public ActionResult Logout()
        { 
            Session.Abandon();  // clear session ทั้งหมดทุกตัว ถ้าเป็น clear เลือก clear เฉพาะบางตัวได้
            return RedirectToAction("Index","Login");
        }

    }
}
