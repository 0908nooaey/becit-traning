﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BECIT.NEOS.Admin.Controllers
{
    public class _BaseController : Controller
    {
        //แก้เรื่อง กดลิ้งหน้าไหน แล้วถ้ายังไม่มี session  
        //หน้าไหนที่มีการ control สิทธิ์ หรือ เรียกใช้งาน controller แม่ของมัน ให้เอา _BaseController ไปใส่แทน
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string currentControllerName = filterContext.Controller.GetType().Name.ToLower().Replace("controller", "");

            if (Session["CurrentUser"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "Controller", "Login" },
                        { "Action", "Index" }
                    });
            }
            else
            {
                CurrentUserModel currentUser = (CurrentUserModel)Session["CurrentUser"];
                var AuthorizeControllers = currentUser.AppModule.Where(x => x.ControllerName != null).Select(x => x.ControllerName.ToLower());
                if (AuthorizeControllers.Contains(currentControllerName) == false)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "Controller", "Login" },
                        { "Action", "Index" }
                    });
                }

            }

            base.OnActionExecuting(filterContext);
        }


    }
}