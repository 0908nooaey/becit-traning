﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BECIT.NEOS.Admin.BusinessLogic;
using Microsoft.Reporting.WebForms;

namespace BECIT.NEOS.Admin.Controllers
{
    public class ReportFileController : _BaseController
    {
        //
        // GET: /ReportFile/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string keyword)
        {
            var reportDatasource = new UserBL().getAll(keyword);

            var reportSource = new UserBL().getAll(null);
            ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();
            rv.ProcessingMode = ProcessingMode.Local;
            rv.LocalReport.ReportPath = Server.MapPath("~/Report/RDLC/UserReport.rdlc");
            rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", reportSource));
            rv.LocalReport.Refresh();

            byte[] streamBytes = null;
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            streamBytes = rv.LocalReport.Render("Excel", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

            return File(streamBytes, mimeType, "UserReport." + filenameExtension);


        }

    }
}
