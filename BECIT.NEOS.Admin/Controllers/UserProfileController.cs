﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserProfileController : Controller
    {
        //
        // GET: /UserProfile/

        public ActionResult Index()
        {
            CurrentUserModel user =  (CurrentUserModel)Session["CurrentUser"];

            ViewBag.userid = user.userid;
            ViewBag.title = user.title;
            ViewBag.fname = user.fname;
            ViewBag.lname = user.lname;
            ViewBag.email = user.email;

        //public DateTime? lastLogin { get; set; }
        //public string AppUserRoleCode { get; set; }
        //public string AppUserRoleDesc { get; set; }
        //public bool HasPermission { get; set; }
        //public string LoginNotifymessage { get; set; } //message ตอน login ไม่ผ่าน
        //public List<AppModuleModel> AppModule { get; set; } //login ผ่าน มีสิทธิ์ module ไหนบ้าง



            return View();
        }

    }
}
