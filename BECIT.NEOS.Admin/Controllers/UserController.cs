﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        //public ActionResult Index(string keyword)
        //{
        //    using (EntitiesNeosDb db = new EntitiesNeosDb())
        //    {
        //        var objModel = (from q in db.APPUSER
        //                        select new UserModel
        //                        {
        //                            USERID = q.USERID,
        //                            TITLE = q.TITLE,
        //                            FNAME = q.FNAME,
        //                            LNAME = q.LNAME,
        //                            LASTLOGIN = q.LASTLOGIN,
        //                            APPUSERROLECODE = q.APPUSERROLECODE,
        //                            APPUSERROLEDesc = q.APPUSERROLE.DESCRIPTION

        //                        }).ToList();

        //        objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.Contains(keyword)).ToList();

        //        ViewBag.keyword = keyword;

        //        return View(objModel);
        //    }

        //}

        public ActionResult Index(int? page,string keyword)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    USERID = q.USERID,
                                    TITLE = q.TITLE,
                                    FNAME = q.FNAME,
                                    LNAME = q.LNAME,
                                    LASTLOGIN = q.LASTLOGIN,
                                    APPUSERROLECODE = q.APPUSERROLECODE,
                                    APPUSERROLEDesc = q.APPUSERROLE.DESCRIPTION

                                }).ToList();

                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.Contains(keyword)).ToList();

                ViewBag.keyword = keyword;

                //Paging Code
                int pageSize = 2;
                int pageNumber = (page ?? 1);
                var listPaged = objModel.ToPagedList(pageNumber, pageSize);

                //End Paging Code

                return View(listPaged);
            }

        }


        [HttpPost]
        public ActionResult Index(string keyword, string dummy)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            return Index(1,keyword);
        }


        public ActionResult InputForm(string id)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            var objModel = new UserModel();
            if (id != null)
            {
                using (EntitiesNeosDb db = new EntitiesNeosDb())
                {
                    var dbObj = (from q in db.APPUSER where q.USERID == id select q).FirstOrDefault();
                    objModel.USERID = dbObj.USERID;
                    objModel.TITLE = dbObj.TITLE;
                    objModel.FNAME = dbObj.FNAME;
                    objModel.LNAME = dbObj.LNAME;
                    objModel.EMAIL = dbObj.EMAIL;
                }
            }

          
            return View(objModel);
        }

        [HttpPost]
        public ActionResult InputForm(UserModel objModel)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            //Insert into DB
            using(EntitiesNeosDb db = new EntitiesNeosDb())
            {
                try
                {

                    var countExistRecord = (from q in db.APPUSER where q.USERID == objModel.USERID select q).Count();

                    if (countExistRecord == 0)
                    {
                        var dbObject = new APPUSER();
                        dbObject.USERID = objModel.USERID;
                        dbObject.TITLE = objModel.TITLE;
                        dbObject.FNAME = objModel.FNAME;
                        dbObject.LNAME = objModel.LNAME;
                        dbObject.EMAIL = objModel.EMAIL;
                        dbObject.APPUSERROLECODE = objModel.APPUSERROLECODE;

                        db.APPUSER.Add(dbObject);
                        db.SaveChanges();
                    }
                    else
                    {
                        var existObject = (from q in db.APPUSER where q.USERID == objModel.USERID select q).FirstOrDefault();
                        existObject.TITLE = objModel.TITLE;
                        existObject.FNAME = objModel.FNAME;
                        existObject.LNAME = objModel.LNAME;
                        existObject.EMAIL = objModel.EMAIL;

                        db.SaveChanges();
                    }

                    var messageResult = new MessageResultModel();
                    messageResult.Style = "alert alert-success";
                    messageResult.Title = "Success!";
                    messageResult.Description = "บันทึกข้อมูลเรียบร้อยแล้ว";
                    TempData["MessageResult"] = messageResult;
                }
                catch (Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;
                    var messageResult = new MessageResultModel();
                    messageResult.Style = "alert alert-danger";
                    messageResult.Title = "Error!";
                    messageResult.Description = "เกิดความผิดพลาด " + exc.Message;
                    TempData["MessageResult"] = messageResult;
                }
            }



           
            return View(objModel);
        }





    }

}
