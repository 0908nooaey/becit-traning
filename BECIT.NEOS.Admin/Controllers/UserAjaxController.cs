﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserAjaxController : _BaseController
    {
        //
        // GET: /UserAjax/

        public ActionResult Index(int? page, string keyword)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSERAJAX";
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    USERID = q.USERID,
                                    TITLE = q.TITLE,
                                    FNAME = q.FNAME,
                                    LNAME = q.LNAME,
                                    LASTLOGIN = q.LASTLOGIN,
                                    APPUSERROLECODE = q.APPUSERROLECODE,
                                    APPUSERROLEDesc = q.APPUSERROLE.DESCRIPTION

                                }).ToList();

                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.Contains(keyword)).ToList();

                ViewBag.keyword = keyword;

                //Paging Code
                int pageSize = 2;
                int pageNumber = (page ?? 1);
                var listPaged = objModel.ToPagedList(pageNumber, pageSize);

                //End Paging Code

                ViewBag.pagedListObj = listPaged;


                return Request.IsAjaxRequest() ? (ActionResult)PartialView("Table") : View();
            }
        }

        [HttpPost]
        public ActionResult Index(string keyword)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSERAJAX";
            return Index(null, keyword);
        }

    }
}
