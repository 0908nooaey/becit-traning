﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class MessageResultModel
    {
        public string Style { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}