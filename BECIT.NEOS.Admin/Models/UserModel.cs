﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class UserModel
    {

        public string USERID { get; set; }
        public string TITLE { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public string EMAIL { get; set; }
        public DateTime? LASTLOGIN { get; set; }

        public string APPUSERROLECODE { get; set; }
        public string APPUSERROLEDesc { get; set; }
    }
}