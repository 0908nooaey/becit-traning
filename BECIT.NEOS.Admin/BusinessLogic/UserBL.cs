﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.BusinessLogic
{
    public class UserBL
    {
        public List<UserModel> getAll(string keyword)
        {
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                //var objModel = (from q in db.APPUSER select q).ToList();


                //ถ้า data เยอะ ค่อยใช้แบบนี้ 
                //var objModel = (from q in db.APPUSER.SqlQuery("Select * from appuser where  userid like '%  " + keyword + "%'")

                //test
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    USERID = q.USERID,
                                    TITLE = q.TITLE,
                                    FNAME = q.FNAME,
                                    LNAME = q.LNAME,
                                    LASTLOGIN = q.LASTLOGIN,
                                    APPUSERROLECODE = q.APPUSERROLECODE,
                                    APPUSERROLEDesc = q.APPUSERROLE.DESCRIPTION
                                }).ToList();

                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.ToLower().Contains(keyword.ToLower())).ToList();
                return objModel;

            }
        }

    }
}